# Inicio de Flask
from flask import Flask, url_for, send_from_directory, request, jsonify
import logging, os
from werkzeug.utils import secure_filename

app = Flask(__name__)
file_handler = logging.FileHandler('server.log')
app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)

PROJECT_HOME = os.path.dirname(os.path.realpath(__file__)) # devuelve ruta de TP2
UPLOAD_FOLDER = '{}/uploads/'.format(PROJECT_HOME) # a la ruta le agrega /uploads
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def create_new_folder(local_dir):
    newpath = local_dir
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    return newpath


# -*- coding: utf-8 -*-
import random # para números aleatorios
import os
import numpy as np
import time
import copy

# Bibliotecas de PyTorch
import torch # operaciones sobre tensores
from torch import nn  #paquete de capas y funciones de activación
from torch import optim  # paquete de optimización
from torch.optim import lr_scheduler # paquete de scheduler
import torchvision

# Conjunto de datos
from torch.utils import data # crear nuevos datasets o iterar sobre uno ya creado
from torchvision import datasets # conjuntos de datos precargados
from torchvision import transforms # transformaciones sobre los datos luego de ser cargados

# Modelos preentrenados
from torchvision import models # cargar diferentes modelos preentrenados

from PIL import Image
import warnings

import pandas as pd
import folium


class_names = ['Juan Pablo II', 'Mirador Bertolotto' , 'Parque Media Luna' , 'Parque de la Huaca' ]

# device = torch.device('cuda' if torch.cuda.is_available() else 'cpu') 

device = torch.device('cpu')

import pickle

filename = 'D:\\API\\modelo\\NModelo' #ruta del modelo

#cargar modelo
loaded_model = pickle.load(open(filename,'rb'))


clases_pred = []
warnings.filterwarnings('ignore')

Ubic = {'LAT':[ -12.076061547124523, -12.093456540247404, -12.092426512238227,-12.08316519791973 ], 'LONG' : [-77.08471052742092, -77.07999545441872,-77.08003911835672 ,-77.09045018603064]}
DF = pd.DataFrame(Ubic)


def predict_image(model, image):
   #carga del modelo
    image = Image.open(image).convert('RGB')
    transform = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])
    image_tensor = transform(image)
    image_tensor = image_tensor.unsqueeze_(0)
    input = image_tensor.to(device)
    output = model(input)
    index = output.data.cpu().numpy().argmax()
    return index


@app.route('/ubicacion', methods = ['POST'])
def api_root():
    app.logger.info(PROJECT_HOME)
    if request.method == 'POST' and request.files['image']:
        app.logger.info(app.config['UPLOAD_FOLDER'])
        img = request.files['image']
        img_name = secure_filename(img.filename)
        create_new_folder(app.config['UPLOAD_FOLDER'])
        saved_path = os.path.join(app.config['UPLOAD_FOLDER'], img_name)
        app.logger.info("saving {}".format(saved_path))
        img.save(saved_path)

        img_route = PROJECT_HOME + '\\uploads\\' + img_name

        indice = predict_image(loaded_model, img_route) #nombre
        place = class_names[indice]
        ubicacion = DF.iloc[indice].to_numpy()

        return jsonify({
            "nombre": place,
            "longitud": str(ubicacion[1]),
            "latitud": str(ubicacion[0])
            })

    else:
        return "Where is the image?"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9090)
